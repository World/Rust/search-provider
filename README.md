# search-provider

Rust wrapper around the GNOME Shell search provider API

- [Project homepage](https://gitlab.gnome.org/World/Rust/search-provider)
- [Latest documentation](https://docs.rs/search-provider/)
- [Nightly documentation](https://world.pages.gitlab.gnome.org/Rust/search-provider/search_provider/)
- [search-provider on crates.io](https://crates.io/crates/search-provider)
- [Search Provider interface](https://developer.gnome.org/documentation/tutorials/search-provider.html)
